#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>

using namespace std;

class QuickSort{
private:
    void swap(int &a, int &b){
        int c = a;
        a = b;
        b = c;
    }

    int med(int a, int b, int c){
        int med = 0;
        if(a>b){
            if(a>c){
                if(c>b){
                    med = c;
                }else{
                    med = b;
                }
            }else{
                med = a;
            }
        }else if(b>c){
            if(c>a){
                med = c;
            }else{
                med = a;
            }
        }else{
            med = b;
        }

        return med;
    }

    int partition(vector<int> &array, int left, int right){
        int pivot = array[left];
        int i = left+1;
        for(int j=left+1; j<=right; j++){
            if(array[j]<pivot){
                swap(array[i], array[j]);
                i++;
            }
        }
        swap(array[left], array[i-1]);
        return i-1;
    }

public:
    int no_of_comparisons = 0;
    void quickSort(vector<int> &array, int left, int right){
        if(left<right){
            no_of_comparisons += right - left;
            //choose pivot.. this will be just the first element in the array for this iteration of the question
            choosePivot(array, left, right);
            int pivotIndex = partition(array, left, right);
            if(pivotIndex == left){
                //this means that there is no left
            }else{
                quickSort(array, left, pivotIndex-1);
            }
            if(pivotIndex == right){
                //there is no right
            }else{
                quickSort(array, pivotIndex+1, right);
            }
        }
    }

    void choosePivot(vector<int> &array, int left, int right){
        //pivot is the left most element of the array
        //return;

        //pivot is the right most element of the array
        //swap(array[left], array[right]);
        //return;

        //pivot is the median of the first middle and the last elements
        int first, second, middle;
        int median = 0;
        int middle_index = 0;
        if((right-left+1) > 2){
            first = array[left];
            second = array[right];
            middle = 0;
            if((right-left+1)%2 == 0){
                middle_index = left + (right-left+1)/2 - 1;
                middle = array[left + (right-left+1)/2 - 1];
            }else{
                middle_index = left + (right-left+1)/2;
                middle = array[left + (right-left+1)/2];
            }
            median = med(first, second, middle);
            if(median == first){
                //no need to do anything
            }else if(median == second){
                swap(array[left], array[right]);
            }else if(median == middle){
                swap(array[left], array[middle_index]);
            }
        }else if((right-left+1) == 2){
            median = array[left];
        }else if((right-left+1) == 1){
            median = array[left];
        }

        //cout<<"                         Median : "<<median<<endl;

    }
};

int main(){
    fstream myfile;
    myfile.open("QuickSort.txt", ios::in);
    vector<int> array;//(a, a+sizeof(a)/sizeof(int));
    while(!myfile.eof()){
        int a;
        myfile >> a;
        array.push_back(a);
    }
    array.pop_back();
    cout<<array.size()<<endl;

    QuickSort quicksort;
    quicksort.quickSort(array, 0, array.size()-1);

    /*
    for(int i=0; i<(int)array.size(); i++){
        cout<<array[i]<<endl;
    }*/

    cout<<endl<<endl;
    cout<<"The total number of comparisons were : "<<quicksort.no_of_comparisons<<endl;
}
